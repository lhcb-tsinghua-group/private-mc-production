from Gaudi.Configuration import *

importOptions("$APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep2015.py")
importOptions("$APPCONFIGOPTS/Moore/MooreSimProductionHlt1.py")
importOptions("$APPCONFIGOPTS/Conditions/TCK-0x51641725.py")
importOptions("$APPCONFIGOPTS/Moore/DataType-2017.py")


from Moore.Configuration import Moore
Moore().outputFile = 'Gausstest_MB_withHLT1.xdigi'

Moore().DDDBtag = "dddb-20170721-3"
Moore().CondDBtag = "sim-20181008-2017pNeBeam2500-vc-md"
Moore().DataType = "2017"

Moore().Simulation=True

from Configurables import Moore
Moore().UseTCK = True
Moore().InitialTCK = '0x51641725' 
Moore().Split = 'Hlt1'# this creates only to run the Hlt1...

Moore().WriterRequires = [ ]

Moore().CheckOdin=False
Moore().ForceSingleL0Configuration = True


from GaudiConf import IOExtension
IOExtension().inputFiles(['Gausstest_MB_withL0.xdigi'],clear=True)

