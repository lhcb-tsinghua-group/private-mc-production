from Gaudi.Configuration import *

from L0App.Configuration import L0App
L0App().outputFile = 'Gausstest_MB_withL0.xdigi'

from Gaudi.Configuration import *
importOptions("$APPCONFIGOPTS/L0App/L0AppSimProduction.py")
importOptions("$APPCONFIGOPTS/L0App/L0AppTCK-0x1725.py")
importOptions("$APPCONFIGOPTS/L0App/ForceLUTVersionV8.py")
importOptions("$APPCONFIGOPTS/L0App/DataType-2017.py")

                                                                               
#L0App().TCK = '0x024E'

L0App().DDDBtag   = "dddb-20170721-3"
L0App().CondDBtag = "sim-20181008-2017pNeBeam2500-vc-md"


#for local running...
from GaudiConf import IOExtension
IOExtension().inputFiles(['Gausstest_MB.xdigi'],clear=True)


L0App().Simulation = True

