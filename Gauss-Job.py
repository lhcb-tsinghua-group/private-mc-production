# Options specific for a given job
# ie. setting of random number seed and name of output files
#

from Configurables import Gauss, LHCbApp

theApp = Gauss()
LHCbApp().DDDBtag="dddb-20170721-3" #pNe 2017
LHCbApp().CondDBtag="sim-20181008-2017pNeBeam2500-vc-md" #pNe 2017

from Gaudi.Configuration import *
importOptions("$LBCRMCROOT/options/EPOS.py") #Setting generator
importOptions("$GAUSSOPTS/BeforeVeloGeometry.py") #Setting Geo
importOptions("$APPCONFIGOPTS/Gauss/DataType-2017.py")
importOptions("$APPCONFIGOPTS/Gauss/RICHRandomHits.py")# Random hits in RICH for occupancy
importOptions("$DECFILESROOT/options/23263020.py") # Event type containing the signal
importOptions("$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py") # Physics simulated by Geant4
importOptions("pNe-Beam2510GeV-0GeV-md100-2017-reducedPVZ.py") # Sets beam energy and position
importOptions("FixThreeInteractionAfterEPOS.py") #DIY 

from Gauss.Configuration import *

#--Generator phase, set random numbers
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 5
GaussGen.RunNumber        = 150

#--Number of events
nEvts = 1500 
LHCbApp().EvtMax = nEvts

#Gauss().OutputType = 'NONE'
#Gauss().Histograms = 'Default'
#--Set name of output files for given job (uncomment the lines)
#  Note that if you do not set it Gauss will make a name based on event type,
#  number of events and the date
#idFile = 'GaussTest'
#HistogramPersistencySvc().OutputFile = idFile+'-histos.root'
#
#OutputStream("GaussTape").Output = "DATAFILE='PFN:/scratch20/pA/Gauss/Gausstest.sim' TYP='POOL_ROOTTREE' OPT='RECREATE'"

OutputStream("GaussTape").Output = "DATAFILE='PFN:Gausstest_MB.sim' TYP='POOL_ROOTTREE' OPT='RECREATE'"

