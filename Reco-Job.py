from Gaudi.Configuration import *
from Configurables import Brunel, LHCbApp
importOptions("$APPCONFIGOPTS/Brunel/DataType-2017.py")
importOptions("$APPCONFIGOPTS/Brunel/PVreco-smog.py")
importOptions("$APPCONFIGOPTS/Brunel/MC-WithTruth.py")

Brunel()
                                                                              
LHCbApp().DDDBtag="dddb-20170721-3"
LHCbApp().CondDBtag="sim-20181008-2017pNeBeam2500-vc-md"
Brunel().DatasetName = 'pNe_MBtest'
Brunel().Simulation = True
Brunel().OutputType='DST'

EventSelector().Input = [
    "DATAFILE='Gausstest_MB_withHLT2.xdigi' TYP='POOL_ROOTTREE' OPT='READ'"
    ]

