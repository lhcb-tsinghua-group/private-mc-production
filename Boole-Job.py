from Gaudi.Configuration import *
from Configurables import Boole, LHCbApp
#import Boole
#from Configurables import LHCbApp


from Gaudi.Configuration import *
importOptions("$APPCONFIGOPTS/Boole/Default.py")
importOptions("$APPCONFIGOPTS/Boole/DataType-2015.py")
importOptions("$APPCONFIGOPTS/Boole/Boole-SetOdinRndTrigger.py")
importOptions("$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py")
importOptions("$APPCONFIGOPTS/Boole/SMOG.py")


#-- File catalogs
#FileCatalog().Catalogs = [ "xmlcatalog_file:NewCatalog.xml" ]

#-- Event input
LHCbApp().DDDBtag   = "dddb-20170721-3"
LHCbApp().CondDBtag = "sim-20181008-2017pNeBeam2500-vc-md"


# Default output files names are set up using value Boole().DatasetName property
Boole().DatasetName = "pNe"
#Comment the following line necessary for local running, but prevents from working for running in batch mode
EventSelector().Input = [ 'Gausstest_MB.sim' ]

# Redefine defaults by uncommenting one or more of options below 
# Monitoring histograms
#HistogramPersistencySvc().OutputFile = "SomeFile.root"

OutputStream("DigiWriter").Output = "DATAFILE='PFN:Gausstest_MB.xdigi' TYP='POOL_ROOTTREE' OPT='REC'"
OutputStream("DigiWriter").OptItemList += [ '/Event/Gen/HepMCEvents#1' ]
